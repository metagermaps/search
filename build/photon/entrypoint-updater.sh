#!/bin/bash

while [ true ]
do
 echo `printf "[%s] Starting update" "\`date\`"`
 curl -s "http://photon:2322/nominatim-update"
 echo ""
 sleep 300
done